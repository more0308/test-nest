
## Installation

```bash
cp .env.example .env
npm install
docker-compose up -d
docker-compose exec server npm run prefill
```
