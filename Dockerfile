FROM node:lts-alpine

WORKDIR /app

COPY ./ /app

RUN apk update && apk add tzdata
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


CMD [ "npm", "run", "start:dev" ]
