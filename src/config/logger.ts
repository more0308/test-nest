import { transports, format, createLogger } from 'winston';

export const winstonConfig = {
  transports: [
    new transports.File({
      filename: `logs/.log`,
      level: 'info',
      format: format.printf((info) => {
        return `${info.message}`;
      }),
    }),
    new transports.Console({
      level: 'debug',
      format: format.combine(
        format.cli(),
        format.splat(),
        format.timestamp(),
        format.printf((info) => {
          return `${info.timestamp} ${info.level}: ${info.message}`;
        }),
      ),
    }),
  ],
};

export const logger = createLogger(winstonConfig);
