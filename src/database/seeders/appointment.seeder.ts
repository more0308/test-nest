import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Doctor } from '../../modules/doctor/doctor.model';
import { Appointment } from '../../modules/appointment/appointment.model';

@Injectable()
export class AppointmentSeeder {
  constructor(
    @InjectModel(Doctor.name) private doctorModel: Model<Doctor>,
    @InjectModel(Appointment.name) private appointmentModel: Model<Appointment>,
  ) {}

  async seed() {
    await this.appointmentModel.deleteMany();

    const appointmentsData = [
      {
        time: this.roundedDate(new Date().setDate(new Date().getDate() + 1)),
        doctor: new Types.ObjectId('65c29f46e4454dffd9e36e2d'),
      },
      {
        time: this.roundedDate(new Date().setDate(new Date().getDate() + 2)),
        doctor: new Types.ObjectId('65c29f46e4454dffd9e36e2d'),
      },
      {
        time: this.roundedDate(new Date().setDate(new Date().getDate() + 3)),
        doctor: new Types.ObjectId('65c29f46e4454dffd9e36e2d'),
      },
    ];

    await this.appointmentModel.create(appointmentsData);
  }

  roundedDate(date: number): Date {
    const originalDate = new Date(date);

    return new Date(Math.floor(originalDate.getTime() / 3600000) * 3600000);
  }
}
