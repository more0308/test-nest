import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Doctor } from '../../modules/doctor/doctor.model';

@Injectable()
export class DoctorSeeder {
  constructor(@InjectModel(Doctor.name) private doctorModel: Model<Doctor>) {}

  async seed() {
    await this.doctorModel.deleteMany();

    const doctorsData = [
      {
        _id: new Types.ObjectId('65c29f46e4454dffd9e36e2d'),
        name: 'Gachi Mychi',
        spec: 'Проктолог',
      },
    ];
    await this.doctorModel.create(doctorsData);
  }
}
