import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../../modules/user/user.model';

@Injectable()
export class UserSeeder {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async seed() {
    await this.userModel.deleteMany();

    const usersData = [
      { name: 'Рикардо Милос', phone: '+380845859322' },
      { name: 'Jane Doe', phone: '+380845859321' },
    ];

    await this.userModel.create(usersData);
  }
}
