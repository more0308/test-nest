import { seeder } from 'nestjs-seeder';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../../modules/user/user.model';
import { UserSeeder } from './user.seeder';
import * as process from 'process';
import { ConfigModule } from '@nestjs/config';
import { DoctorSeeder } from './doctor.seeder';
import { Doctor, DoctorSchema } from '../../modules/doctor/doctor.model';
import { AppointmentSeeder } from './appointment.seeder';
import {
  Appointment,
  AppointmentSchema,
} from '../../modules/appointment/appointment.model';

seeder({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env' }),
    MongooseModule.forRoot(process.env.MONGO_URI),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: Doctor.name, schema: DoctorSchema }]),
    MongooseModule.forFeature([
      { name: Appointment.name, schema: AppointmentSchema },
    ]),
  ],
}).run([UserSeeder, DoctorSeeder, AppointmentSeeder]);
