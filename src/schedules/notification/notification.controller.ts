import { Controller } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { NotificationService } from './notification.service';
import { AppointmentService } from '../../modules/appointment/appointment.service';
import { Appointment } from '../../modules/appointment/appointment.model';

@Controller('notification')
export class NotificationController {
  constructor(
    private appointmentService: AppointmentService,
    private service: NotificationService,
  ) {}

  @Cron('0 0 * * * *')
  async oneHourNoticeClients() {
    const currentDateTime = new Date();
    const timeInTwoHours = new Date(currentDateTime);
    timeInTwoHours.setHours(currentDateTime.getHours() + 2);

    const appointments =
      await this.appointmentService.getAppointments(timeInTwoHours);

    appointments.forEach((appointment: Appointment) => {
      this.service.reportInTwoHours(appointment);
    });
  }

  @Cron('0 0 * * * *')
  async oneDayNoticeClients() {
    const currentDateTime = new Date();
    const timeInTwoHours = new Date(currentDateTime);
    timeInTwoHours.setHours(currentDateTime.getHours() + 24);

    const appointments =
      await this.appointmentService.getAppointments(timeInTwoHours);

    appointments.forEach((appointment: Appointment) => {
      this.service.reportInDay(appointment);
    });
  }
}
