import { Module } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { NotificationController } from './notification.controller';
import { AppointmentModule } from '../../modules/appointment/appointment.module';

@Module({
  providers: [NotificationService],
  controllers: [NotificationController],
  imports: [AppointmentModule],
})
export class NotificationModule {}
