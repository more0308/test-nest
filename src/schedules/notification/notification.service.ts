import { Injectable, Logger } from '@nestjs/common';
import { Appointment } from '../../modules/appointment/appointment.model';

@Injectable()
export class NotificationService {
  private readonly logger = new Logger(NotificationService.name);

  reportInTwoHours(data: Appointment) {
    const currentDate = new Date();

    this.logger.log(
      `${currentDate.toLocaleString()} | Привет ${data.user.name}! Вам через 2 часа к ${data.doctor.spec} в ${data.time.toLocaleString()}!`,
    );
  }
  reportInDay(data: Appointment) {
    const currentDate = new Date();

    this.logger.log(
      `${currentDate.toLocaleString()} | Привет ${data.user.name}! Напоминаем что вы записаны к ${data.doctor.spec} завтра в ${data.time.toLocaleString()}!`,
    );
  }
}
