import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as process from 'process';
import { logger } from './config/logger';
async function app() {
  const PORT = process.env.PORT || 5000;
  const app = await NestFactory.create(AppModule, {
    logger,
  });
  await app.listen(PORT, () => console.log(`Running on port ${PORT}`));
}

app();
