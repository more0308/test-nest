import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './modules/user/user.module';
import { DoctorModule } from './modules/doctor/doctor.module';
import * as process from 'process';
import { ScheduleModule } from '@nestjs/schedule';
import { NotificationModule } from './schedules/notification/notification.module';
import { AppointmentModule } from './modules/appointment/appointment.module';

@Module({
  controllers: [],
  providers: [],
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env' }),
    MongooseModule.forRoot(process.env.MONGO_URI),
    ScheduleModule.forRoot(),
    UserModule,
    DoctorModule,
    NotificationModule,
    AppointmentModule,
  ],
})
export class AppModule {}
