import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Doctor } from './doctor.model';

@Injectable()
export class DoctorService {
  constructor(@InjectModel(Doctor.name) private model: Model<Doctor>) {}

  async get(id: string): Promise<Doctor> {
    return this.model.findById(id);
  }
}
