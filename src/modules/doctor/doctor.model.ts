import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Doctor {
  @Prop()
  name: string;

  @Prop()
  spec: string;
}
export const DoctorSchema = SchemaFactory.createForClass(Doctor);
