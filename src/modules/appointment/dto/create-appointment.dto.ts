import { IsDate, IsNotEmpty, IsString } from 'class-validator';

export class CreateAppointmentDto {
  @IsString()
  @IsNotEmpty()
  user_id: string;

  @IsString()
  @IsNotEmpty()
  doctor_id: string;

  @IsDate()
  @IsNotEmpty()
  time: Date;
}
