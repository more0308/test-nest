import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { DoctorService } from '../doctor/doctor.service';
import { UserService } from '../user/user.service';
import { CreateAppointmentDto } from './dto/create-appointment.dto';
import { AppointmentService } from './appointment.service';

@Controller('appointment')
export class AppointmentController {
  constructor(
    private service: AppointmentService,
    private doctorService: DoctorService,
    private userService: UserService,
  ) {}

  @Post()
  async create(@Body() dto: CreateAppointmentDto) {
    const user = await this.userService.get(dto.user_id);
    const doctor = await this.doctorService.get(dto.doctor_id);
    if (!user || !doctor) {
      throw new BadRequestException('Incorrect data');
    }

    const appointment = await this.service.create(dto);

    if (!appointment) {
      throw new BadRequestException('Incorrect time');
    }

    return dto;
  }
}
