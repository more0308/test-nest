import { Injectable } from '@nestjs/common';
import { CreateAppointmentDto } from './dto/create-appointment.dto';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Appointment } from './appointment.model';

@Injectable()
export class AppointmentService {
  constructor(
    @InjectModel(Appointment.name) private model: Model<Appointment>,
  ) {}

  async create(dto: CreateAppointmentDto): Promise<Appointment> {
    const date = this.roundedDate(dto.time);
    return this.model.findOneAndUpdate(
      {
        doctor: new Types.ObjectId(dto.doctor_id),
        time: date,
        user: null,
      },
      {
        $set: {
          user: dto.user_id,
        },
      },
    );
  }

  roundedDate(date: Date): Date {
    const originalDate = new Date(date);

    return new Date(Math.floor(originalDate.getTime() / 3600000) * 3600000);
  }

  async getAppointments(time: Date): Promise<any> {
    const date = this.roundedDate(time);

    return this.model
      .find({
        time: date,
        user: { $ne: null },
      })
      .populate('user')
      .populate('doctor');
  }
}
