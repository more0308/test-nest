import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from '../user/user.model';
import mongoose from 'mongoose';
import { Doctor } from '../doctor/doctor.model';

@Schema()
export class Appointment {
  @Prop({ type: Date })
  time: Date;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Doctor' })
  doctor: Doctor;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false })
  user?: User;
}
export const AppointmentSchema = SchemaFactory.createForClass(Appointment);
