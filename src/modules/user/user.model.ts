import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class User {
  @Prop()
  name: string;

  @Prop()
  phone: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
